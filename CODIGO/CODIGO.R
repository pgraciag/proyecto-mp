library(tidyr)
library(dplyr)
directorio <- "C:\\Users\\Pablo\\Desktop\\Proyecto"
setwd(directorio)
Fuente<-list.files("C:.\\DATOS\\BRUTOS\\CSV\\FUENTES")
Mes<-list.files("C:.\\DATOS\\BRUTOS\\CSV\\MESES")
Edad<-list.files("C:.\\DATOS\\BRUTOS\\CSV\\EDADES")
Fuente
Mes
Edad
Archivo=vector(mode="list")
Archivo2=vector(mode="list")
Archivo3=vector(mode = "list")
prueba=vector(mode = "list")
prueba2=vector(mode = "list")
prueba3=vector(mode="list")
for(i in 1:length(Fuente)){
  Archivo[[i]]<-read.csv(paste(directorio, "\\DATOS\\BRUTOS\\CSV\\FUENTES\\",Fuente[i], sep=''))
  prueba[[i]]<-(gather(Archivo[[i]], key= "Fuente","Casos",2:10))
  prueba[[i]]$Casos<-as.character(prueba[[i]]$Casos)
  prueba[[i]]<-within(prueba[[i]],{
    Estado<-gsub("Nuev.*","Nuevo Leon",Estado)
    Estado<-gsub("Micho.*","Michoacan",Estado)
    Estado<-gsub("Quer.*","Queretaro",Estado)
    Estado<-gsub("Yuca.*","Yucatan",Estado)
    Estado<-gsub("San.*","San Luis Potosi",Estado)
    Estado<-gsub("M.*xico","Mexico",Estado)
    Fuente<-gsub("SSA.*","Salud",Fuente)
    Casos<-gsub("S/R.*|N.A.*|S.R.*","NA",Casos)
  })
}

for(i in 1:length(Mes)){
  Archivo2[[i]]<-assign(Mes[i],
                        read.csv(paste(directorio, "\\DATOS\\BRUTOS\\CSV\\MESES\\", Mes[i], sep='')))
  prueba2[[i]]<- gather(Archivo2[[i]], key= "Meses",value="Casos",2:13)
  prueba2[[i]]$Casos<-as.character(prueba2[[i]]$Casos)
  prueba2[[i]]<-within(prueba2[[i]],{
    Estado<-gsub("Nuev.*","Nuevo Leon",Estado)
    Estado<-gsub("Micho.*","Michoacan",Estado)
    Estado<-gsub("Quer.*","Queretaro",Estado)
    Estado<-gsub("Yuca.*","Yucatan",Estado)
    Estado<-gsub("San.*","San Luis Potosi",Estado)
    Estado<-gsub("M.*xico","Mexico",Estado)
    
  })
}
for(i in 1:length(Edad)){
  Archivo3[[i]]<-assign(Edad[i],
                        read.csv(paste(directorio, "\\DATOS\\BRUTOS\\CSV\\EDADES\\", Edad[i], sep='')))
  prueba3[[i]]<- gather(Archivo3[[i]], key= "Rango",value="Casos",2:13)
  prueba3[[i]]$Casos<-as.character(prueba3[[i]]$Casos)
  prueba3[[i]]<-within(prueba3[[i]],{
    Estado<-gsub("Nuev.*","Nuevo Leon",Estado)
    Estado<-gsub("Micho.*","Michoacan",Estado)
    Estado<-gsub("Quer.*","Queretaro",Estado)
    Estado<-gsub("Yuca.*","Yucatan",Estado)
    Estado<-gsub("San.*","San Luis Potosi",Estado)
    Estado<-gsub("M.*xico","Mexico",Estado)
  })
}
FuenteF=vector(mode = "list")
FuenteG=vector(mode = "list")
FuenteM=vector(mode = "list")
MesF=vector(mode = "list")
MesG=vector(mode = "list")
MesM=vector(mode = "list")
EdadF=vector(mode = "list")
EdadG=vector(mode = "list")
EdadM=vector(mode = "list")
i=1
k=1
j=1
for(i in c(1,4,7,10,13,16,19,22,25,28,31,34)){
  sexo<-rep("Femenino")
  FuenteF[[i]] <- data.frame(prueba[[i]],sexo)
  MesF[[i]]<-data.frame(prueba2[[i]],sexo)
  EdadF[[i]]<-data.frame(prueba3[[i]],sexo)
  if(j<3){
    A�o<-rep(2004)
    FuenteF[[i]]<-data.frame(FuenteF[[i]],A�o)
    MesF[[i]]<-data.frame(MesF[[i]],A�o)
    EdadF[[i]]<-data.frame(EdadF[[i]],A�o)
    j=j+3
  }
  else{
    A�o<-rep(2004+k)
    FuenteF[[i]]<-data.frame(FuenteF[[i]],A�o)
    MesF[[i]]<-data.frame(MesF[[i]],A�o)
    EdadF[[i]]<-data.frame(EdadF[[i]],A�o)
    k=k+1
  }
}
i=1
k=1
j=1
for(i in c(2,5,8,11,14,17,20,23,26,29,32,35)){
  sexo<-rep("General")
  FuenteG[[i]] <- data.frame(prueba[[i]],sexo)
  MesG[[i]]<-data.frame(prueba2[[i]],sexo)
  EdadG[[i]]<-data.frame(prueba3[[i]],sexo)
  if(j<3){
    A�o<-rep(2004)
    FuenteG[[i]]<-data.frame(FuenteG[[i]],A�o)
    MesG[[i]]<-data.frame(MesG[[i]],A�o)
    EdadG[[i]]<-data.frame(EdadG[[i]],A�o)
    j=j+3
  }
  else{
    A�o<-rep(2004+k)
    FuenteG[[i]]<-data.frame(FuenteG[[i]],A�o)
    MesG[[i]]<-data.frame(MesG[[i]],A�o)
    EdadG[[i]]<-data.frame(EdadG[[i]],A�o)
    k=k+1
  }
}
i=1
k=1
j=1
for(i in c(3,6,9,12,15,18,21,24,27,30,33,36)){
  sexo<-rep("Mascuino")
  FuenteM[[i]] <- data.frame(prueba[[i]],sexo)
  MesM[[i]]<-data.frame(prueba2[[i]],sexo)
  EdadM[[i]]<-data.frame(prueba3[[i]],sexo)
  if(j<3){
    A�o<-rep(2004)
    FuenteM[[i]]<-data.frame(FuenteM[[i]],A�o)
    MesM[[i]]<-data.frame(MesM[[i]],A�o)
    EdadM[[i]]<-data.frame(EdadM[[i]],A�o)
    j=j+3
  }
  else{
    A�o<-rep(2004+k)
    FuenteM[[i]]<-data.frame(FuenteM[[i]],A�o)
    MesM[[i]]<-data.frame(MesM[[i]],A�o)
    EdadM[[i]]<-data.frame(EdadM[[i]],A�o)
    k=k+1
  }
}
FuenteT=vector(mode = "list")
MesT=vector(mode = "list")
EdadT=vector(mode = "list")
FuenteFF<-FuenteF
MesFF<-MesF
EdadFF<-EdadF
FuenteGF<-FuenteG
MesGF<-MesG
EdadGF<-EdadG
FuenteMF<-FuenteM
MesMF<-MesM
EdadMF<-EdadM
FuenteT<-c(FuenteFF,FuenteGF,FuenteMF)
MesT<-c(MesFF,MesGF,MesMF)
EdadT<-c(EdadFF,EdadGF,EdadMF)
DatosLimpios<-bind_rows(FuenteT)
DatosLimpios2<-bind_rows(MesT)
DatosLimpios3<-bind_rows(EdadT)
DatosLimpiosFuente=DatosLimpios[,c(5,1,4,2,3)]
DatosLimpiosMes=DatosLimpios2[,c(5,1,4,2,3)]
DatosLimpiosEdad=DatosLimpios3[,c(5,1,4,2,3)]
DatosLimpiosFuente$Casos<-as.integer(DatosLimpiosFuente$Casos)
DatosLimpiosMes$Casos<-as.integer(DatosLimpiosMes$Casos)
DatosLimpiosEdad$Casos<-as.integer(DatosLimpiosEdad$Casos)
write.table(DatosLimpiosFuente, file = "C:.\\DATOS\\PROCESADOS\\FuenteLimpios.csv", sep = ",", row.names = FALSE)
write.table(DatosLimpiosMes, file = "C:.\\DATOS\\PROCESADOS\\MesLimpios.csv", sep = ",", row.names = FALSE)
write.table(DatosLimpiosEdad, file = "C:.\\DATOS\\PROCESADOS\\EdadLimpios.csv", sep = ",", row.names = FALSE)
sum(is.na(DatosLimpiosFuente$Casos))
sum(is.na(DatosLimpiosMes$Casos))
sum(is.na(DatosLimpiosEdad$Casos))

CasosF <- 1:10368
CasosM <- 1:10368
CasosG <- 1:10368
CasosE <- 1:10368

for(i in 1:10368)
  CasosF[i]<-(DatosLimpiosFuente$Casos[i])

CasosFemF04<-sum(CasosF[1:288])
CasosFemF05<-sum(CasosF[289:576])
CasosFemF06<-sum(CasosF[577:864])
CasosFemF07<-sum(CasosF[865:1152])
CasosFemF08<-sum(CasosF[1153:1440])
CasosFemF09<-sum(CasosF[1441:1728])
CasosFemF10<-sum(CasosF[1729:2016])
CasosFemF11<-sum(CasosF[2017:2304])
CasosFemF12<-sum(CasosF[2305:2592])
CasosFemF13<-sum(CasosF[2593:2880])
CasosFemF14<-sum(CasosF[2881:3168])
CasosFemF15<-sum(CasosF[3169:3456])
CasosGenF04<-sum(CasosF[3457:3744])
CasosGenF05<-sum(CasosF[3745:4032])
CasosGenF06<-sum(CasosF[4033:4320])
CasosGenF07<-sum(CasosF[4321:4608])
CasosGenF08<-sum(CasosF[4601:4896])
CasosGenF09<-sum(CasosF[4889:5184])
CasosGenF10<-sum(CasosF[5177:5472])
CasosGenF11<-sum(CasosF[5465:5760])
CasosGenF12<-sum(CasosF[5753:6048])
CasosGenF13<-sum(CasosF[6041:6336])
CasosGenF14<-sum(CasosF[6329:6624])
CasosGenF15<-sum(CasosF[6617:6912])
CasosMasF04<-sum(CasosF[6913:7200])
CasosMasF05<-sum(CasosF[7201:7488])
CasosMasF06<-sum(CasosF[7489:7776])
CasosMasF07<-sum(CasosF[7777:8064])
CasosMasF08<-sum(CasosF[8065:8352])
CasosMasF09<-sum(CasosF[8353:8640])
CasosMasF10<-sum(CasosF[8641:8928])
CasosMasF11<-sum(CasosF[8929:9216])
CasosMasF12<-sum(CasosF[9217:9504])
CasosMasF13<-sum(CasosF[9505:9792])
CasosMasF14<-sum(CasosF[9793:10080])
CasosMasF15<-sum(CasosF[10081:10368])

(CasosFemF04+CasosMasF04)==CasosGenF04
(CasosFemF05+CasosMasF05)==CasosGenF05
(CasosFemF06+CasosMasF06)==CasosGenF06
(CasosFemF07+CasosMasF07)==CasosGenF07
(CasosFemF08+CasosMasF08)==CasosGenF08
(CasosFemF09+CasosMasF09)==CasosGenF09
(CasosFemF10+CasosMasF10)==CasosGenF10
(CasosFemF11+CasosMasF11)==CasosGenF11
(CasosFemF12+CasosMasF12)==CasosGenF12
(CasosFemF13+CasosMasF13)==CasosGenF13
(CasosFemF14+CasosMasF14)==CasosGenF14
(CasosFemF15+CasosMasF15)==CasosGenF15


for(i in 1:13824)
  CasosM[[i]]<-(DatosLimpiosMes$Casos[i])
CasosFemM04<-sum(CasosM[1:384])
CasosFemM05<-sum(CasosM[385:768])
CasosFemM06<-sum(CasosM[769:1152])
CasosFemM07<-sum(CasosM[1153:1536])
CasosFemM08<-sum(CasosM[1537:1920])
CasosFemM09<-sum(CasosM[1921:2304])
CasosFemM10<-sum(CasosM[2305:2688])
CasosFemM11<-sum(CasosM[2689:3072])
CasosFemM12<-sum(CasosM[3073:3456])
CasosFemM13<-sum(CasosM[3457:3840])
CasosFemM14<-sum(CasosM[3841:4224])
CasosFemM15<-sum(CasosM[4225:4608])
CasosGenM04<-sum(CasosM[4609:4992])
CasosGenM05<-sum(CasosM[4993:5376])
CasosGenM06<-sum(CasosM[5377:5760])
CasosGenM07<-sum(CasosM[5761:6144])
CasosGenM08<-sum(CasosM[6145:6528])
CasosGenM09<-sum(CasosM[6529:6912])
CasosGenM10<-sum(CasosM[6913:7296])
CasosGenM11<-sum(CasosM[7297:7680])
CasosGenM12<-sum(CasosM[7681:8064])
CasosGenM13<-sum(CasosM[8065:8448])
CasosGenM14<-sum(CasosM[8449:8832])
CasosGenM15<-sum(CasosM[8833:9216])
CasosMasM04<-sum(CasosM[9217:9600])
CasosMasM05<-sum(CasosM[9601:9984])
CasosMasM06<-sum(CasosM[9985:10368])
CasosMasM07<-sum(CasosM[10369:10752])
CasosMasM08<-sum(CasosM[10753:11136])
CasosMasM09<-sum(CasosM[11137:11520])
CasosMasM10<-sum(CasosM[11521:11904])
CasosMasM11<-sum(CasosM[11905:12288])
CasosMasM12<-sum(CasosM[12289:12672])
CasosMasM13<-sum(CasosM[12673:13056])
CasosMasM14<-sum(CasosM[13057:13440])
CasosMasM15<-sum(CasosM[13441:13824])

(CasosFemM04+CasosMasM04)==CasosGenM04
(CasosFemM05+CasosMasM05)==CasosGenM05
(CasosFemM06+CasosMasM06)==CasosGenM06
(CasosFemM07+CasosMasM07)==CasosGenM07
(CasosFemM08+CasosMasM08)==CasosGenM08
(CasosFemM09+CasosMasM09)==CasosGenM09
(CasosFemM10+CasosMasM10)==CasosGenM10
(CasosFemM11+CasosMasM11)==CasosGenM11
(CasosFemM12+CasosMasM12)==CasosGenM12
(CasosFemM13+CasosMasM13)==CasosGenM13
(CasosFemM14+CasosMasM14)==CasosGenM14
(CasosFemM15+CasosMasM15)==CasosGenM15

for(i in 1:13824)
  CasosE[[i]]<-(DatosLimpiosEdad$Casos[i])
CasosFemE04<-sum(CasosM[1:384])
CasosFemE05<-sum(CasosM[385:768])
CasosFemE06<-sum(CasosM[769:1152])
CasosFemE07<-sum(CasosM[1153:1536])
CasosFemE08<-sum(CasosM[1537:1920])
CasosFemE09<-sum(CasosM[1921:2304])
CasosFemE10<-sum(CasosM[2305:2688])
CasosFemE11<-sum(CasosM[2689:3072])
CasosFemE12<-sum(CasosM[3073:3456])
CasosFemE13<-sum(CasosM[3457:3840])
CasosFemE14<-sum(CasosM[3841:4224])
CasosFemE15<-sum(CasosM[4225:4608])
CasosGenE04<-sum(CasosM[4609:4992])
CasosGenE05<-sum(CasosM[4993:5376])
CasosGenE06<-sum(CasosM[5377:5760])
CasosGenE07<-sum(CasosM[5761:6144])
CasosGenE08<-sum(CasosM[6145:6528])
CasosGenE09<-sum(CasosM[6529:6912])
CasosGenE10<-sum(CasosM[6913:7296])
CasosGenE11<-sum(CasosM[7297:7680])
CasosGenE12<-sum(CasosM[7681:8064])
CasosGenE13<-sum(CasosM[8065:8448])
CasosGenE14<-sum(CasosM[8449:8832])
CasosGenE15<-sum(CasosM[8833:9216])
CasosMasE04<-sum(CasosM[9217:9600])
CasosMasE05<-sum(CasosM[9601:9984])
CasosMasE06<-sum(CasosM[9985:10368])
CasosMasE07<-sum(CasosM[10369:10752])
CasosMasE08<-sum(CasosM[10753:11136])
CasosMasE09<-sum(CasosM[11137:11520])
CasosMasE10<-sum(CasosM[11521:11904])
CasosMasE11<-sum(CasosM[11905:12288])
CasosMasE12<-sum(CasosM[12289:12672])
CasosMasE13<-sum(CasosM[12673:13056])
CasosMasE14<-sum(CasosM[13057:13440])
CasosMasE15<-sum(CasosM[13441:13824])

(CasosFemE04+CasosMasE04)==CasosGenE04
(CasosFemE05+CasosMasE05)==CasosGenE05
(CasosFemE06+CasosMasE06)==CasosGenE06
(CasosFemE07+CasosMasE07)==CasosGenE07
(CasosFemE08+CasosMasE08)==CasosGenE08
(CasosFemE09+CasosMasE09)==CasosGenE09
(CasosFemE10+CasosMasE10)==CasosGenE10
(CasosFemE11+CasosMasE11)==CasosGenE11
(CasosFemE12+CasosMasE12)==CasosGenE12
(CasosFemE13+CasosMasE13)==CasosGenE13
(CasosFemE14+CasosMasE14)==CasosGenE14
(CasosFemE15+CasosMasE15)==CasosGenE15

